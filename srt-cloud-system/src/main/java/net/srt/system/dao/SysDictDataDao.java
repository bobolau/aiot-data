package net.srt.system.dao;

import net.srt.framework.mybatis.dao.BaseDao;
import net.srt.system.entity.SysDictDataEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 字典数据
 *
 * @author 阿沐 babamu@126.com
 */
@Mapper
public interface SysDictDataDao extends BaseDao<SysDictDataEntity> {

}
