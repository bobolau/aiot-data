package net.srt.framework.common.config.nacos;

import org.springframework.cloud.context.restart.RestartEndpoint;
import org.springframework.stereotype.Service;

/**
 * @ClassName CustomRestartEndpoint
 * @Author zrx
 * @Date 2023/10/27 14:49
 */
@Service
public class CustomRestartEndpoint extends RestartEndpoint {
}
