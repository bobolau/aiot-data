package cc.iotkit.data.service;

import cc.iotkit.data.model.TbSysOss;
import com.baomidou.mybatisplus.extension.service.IService;

public interface SysOssService extends IService<TbSysOss> {
}
